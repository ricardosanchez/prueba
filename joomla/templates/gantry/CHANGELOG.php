<?php
/**
* @package    CDU Common Template
* @author     Christoph Hagedorn
* @copyright  Copyright © 2007 Free Software Foundation, Inc. <http://fsf.org />
* @license    GNU/GPL, see license.php
*
* @author    RocketTheme http://www.rockettheme.com
* @copyright Copyright (C) 2007 - 2014 RocketTheme, LLC
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
**
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.*
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
* CDU Common Template uses the Gantry Framework (http://www.rockettheme.com), a GNU/GPLv2 web platform theme framework
* Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
**/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted index access' );

<h3>V1.1.1</h3>
<p><ul>
  <li>fixed sticky menu bug on mobile devices</li>
</ul></p>
?>

/**
* @package    CDU Common Template
* @author     Christoph Hagedorn
* @copyright  Copyright © 2007 Free Software Foundation, Inc. <http://fsf.org />
* @license    GNU/GPL, see license.php
*
* snippet taken from
* @author Thoriq Firdaus, www.hongkiat.com
**
* CDU Common Template uses the Gantry Framework (http://www.rockettheme.com), a GNU/GPLv2 web platform theme framework
* Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
**/

((function($){
	$(document).ready(function(){
		var stickyNavTop = $('#rt-header-surround-pattern').offset().top;

		var stickyNav = function(){
			var scrollTop = $(window).scrollTop();
			//var isMobile = window.matchMedia("only screen and (max-width: 760px)");
			var isMobile = window.matchMedia("(max-width: 767px)");

			if ( !isMobile.matches && scrollTop > stickyNavTop) {
				$('#rt-header-surround-pattern').addClass('rt-sticky');
			} else {
				$('#rt-header-surround-pattern').removeClass('rt-sticky');
			}
		};
		stickyNav();
		$(window).scroll(function(){
			stickyNav();
		});
	});
} )(jQuery));
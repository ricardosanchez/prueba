<?php
/**
* @package    CDU Common Template
* @author     Christoph Hagedorn
* @copyright  Copyright © 2007 Free Software Foundation, Inc. <http://fsf.org />
* @license    GNU/GPL, see license.php
*
 * @version   $Id: analytics.php 19328 2014-02-28 18:53:02Z btowles $
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2014 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
**
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.*
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
* CDU Common Template uses the Gantry Framework (http://www.rockettheme.com), a GNU/GPLv2 web platform theme framework
* Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
**/
/**

 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */

defined('JPATH_BASE') or die();

gantry_import('core.gantryfeature');

/**
 * @package     gantry
 * @subpackage  features
 */
class GantryFeatureAnalytics extends GantryFeature
{

	var $_feature_name = 'analytics';

	function init()
	{
		/** @var $gantry Gantry */
		global $gantry;

		ob_start();
		// start of Google Analytics javascript
	?>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');
		__gaTracker('create', '<?php echo $this->get('code'); ?>', 'auto');
		__gaTracker('set', 'anonymizeIp', 'true');
		__gaTracker('send', 'pageview');
	<?php
		// end of Google Analytics javascript
		$gantry->addInlineScript(ob_get_clean());
	}
}
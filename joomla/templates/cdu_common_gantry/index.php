<?php
/**
* @package    CDU Common Gantry Template
* @author     Christoph Hagedorn (christoph.hagedorn@cduplus.de)
* @copyright  Copyright © 2014-2015 Free Software Foundation, Inc. <http://fsf.org />
* @license    GNU/GPL, see license.php
*
* @author    RocketTheme http://www.rockettheme.com
* @copyright Copyright (C) 2007 - 2014 RocketTheme, LLC
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
**
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.*
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
* CDU Common Template uses the Gantry Framework (http://www.rockettheme.com), a GNU/GPLv2 web platform theme framework
* Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
**/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted index access' );

// load and inititialize gantry class
require_once(dirname(__FILE__) . '/lib/gantry/gantry.php');
$gantry->init();

// get the current preset
$gpreset = str_replace(' ','',strtolower($gantry->get('name')));

$app = JFactory::getApplication();
$doc = JFactory::getDocument();


function getCduLogoPath() {

	global $gantry;

        $source = "init";

	if ($gantry->get('logo-type')=="custom"){
	    // get path of custom logo
            $logo = str_replace("&quot;", '"', str_replace("'", '"', $gantry->get('logo-custom-image')));
            if (strlen($logo)) $source = $logo;
	}
	else{
	    // get name of choosen predefined logo
            $logo = str_replace("&quot;", '"', str_replace("'", '"', $gantry->get('logo-type')));
            if( $logo == "cdu-juelich"){
		$source = JURI::root() . ltrim($gantry->templateUrl, '/') . "/images/logo/cdu_social.png";
            }
	    else{
		$source = JURI::root() . ltrim($gantry->templateUrl, '/') . "/images/logo/cdu_social.png";
	    }
	}

	return $source;
}

?><!DOCTYPE html>
<html xml:lang="<?php echo $gantry->language; ?>" lang="<?php echo $gantry->language;?>" >
<head prefix="og: http://ogp.me/ns#">
<?php if ($gantry->browser->name == 'ie') : ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<?php endif; ?>
	<link href="<?php echo $gantry->templateUrl; ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="<?php echo $gantry->templateUrl; ?>/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
        <link href="<?php echo $gantry->templateUrl; ?>/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/png"/>

	<?php if ($gantry->get('layout-mode') == 'responsive') : ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php elseif ($gantry->get('layout-mode') == '960fixed') : ?>
	<meta name="viewport" content="width=960px, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<?php elseif ($gantry->get('layout-mode') == '1200fixed') : ?>
	<meta name="viewport" content="width=1200px, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<?php else : ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php endif; ?>


	<meta name="publisher" content="<?php echo $app->getCfg('sitename'); ?>" />
<!--
	<link href="https://plus.google.com/<?php  echo $gantry->get('GplusId'); ?>" rel="publisher" />
-->
	<meta property="og:locale" content="de_DE" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="<?php echo $doc->getMetaData('description'); ?>" />
	<meta property="og:title" content="<?php echo $doc->getTitle(); ?>" />
	<meta property="og:site_name" content="<?php echo $app->getCfg('sitename'); ?>" />
	<meta property="og:url" content="<?php echo JURI::current(); ?>" />

	<meta property="og:image" content="<?php echo getCduLogoPath(); ?>" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="441" />
	<meta property="og:image:height" content="441" />
	<!--
		Christlich Demokratische Union Deutschlands (www.cdu.de)
		CDU Common Gantry Template for Joomla!
		Copyright 2011-2015 Christoph Hagedorn (christoph.hagedorn@cduplus.de)
	-->

	<!-- CSS optimization above the fold -->
	<style type="text/css">
		body.layout-mode-responsive {
			position: relative;
			transition: left 0.2s ease-out 0s;
		}
		.font-size-is-default {
			font-size: 100%;
			line-height: normal;
		}
		.font-family-cdukievit {
			font-family: "CDU Kievit",Verdana,"Nimbus Sans L",FreeSans,sans-serif;
			font-style: normal;
			font-weight: normal;
		}
		body {
			background: none repeat scroll 0 0 #fefefe;
			color: #000000;
		}
		article, aside, details, figcaption, figure, footer, header, hgroup, nav, section {
			display: block;
		}
		#rt-top-surround:after {
			bottom: 0;
			content: "";
			left: 0;
			position: absolute;
			right: 0;
			top: 0;
			z-index: 0;
		}
		#rt-showcase, #rt-top-surround {
			color: #ffffff;
		}
		#rt-top-surround {
			position: relative;
			min-height: 10px;
			box-shadow: 0 0 4px rgba(0, 0, 0, 0.4);
		}
		#rt-top-surround-pattern {
			background-color: #e4e2d2;
		}
		#rt-header-surround {
			background-color: rgba(255, 153, 0, 0.88);
			color: #fefefe;
			border-bottom: 5px solid #e95d0f;
		}

		.rt-container {
			margin: 0 auto;
			width: 1200px;
		}
		#rt-main {
			background: none repeat scroll 0 0 #ffffff;
		}
		.rt-grid-1:before, .rt-grid-2:before, .rt-grid-3:before, .rt-grid-4:before, .rt-grid-5:before, .rt-grid-6:before, .rt-grid-7:before, .rt-grid-8:before, .rt-grid-9:before, .rt-grid-10:before, .rt-grid-11:before, .rt-grid-12:before {
			clear: both;
			content: "";
			display: table;
		}
		.rt-grid-1:after, .rt-grid-2:after, .rt-grid-3:after, .rt-grid-4:after, .rt-grid-5:after, .rt-grid-6:after, .rt-grid-7:after, .rt-grid-8:after, .rt-grid-9:after, .rt-grid-10:after, .rt-grid-11:after, .rt-grid-12:after {
			clear: both;
			content: "";
			display: table;
		}
		body [class*="rt-grid"] {
			display: inline;
			float: left;
			margin: 0;
			position: relative;
		}
		.rt-push-2 {
			left: 200px;
		}
		.rt-grid-8 {
			width: 800px;
		}
	</style>
<?php
        $gantry->displayHead();
	$gantry->addStyle('grid-responsive.css', 5);
	$gantry->addLess('bootstrap.less', 'bootstrap.css', 6);
        if ($gantry->browser->name == 'ie'){
        	if ($gantry->browser->shortversion == 9){
        		$gantry->addInlineScript("if (typeof RokMediaQueries !== 'undefined') window.addEvent('domready', function(){ RokMediaQueries._fireEvent(RokMediaQueries.getQuery()); });");
        	}
		if ($gantry->browser->shortversion == 8){
			$gantry->addScript('html5shim.js');
		}
	}

	if ($gantry->get('layout-mode', 'responsive') == 'responsive'){
		$gantry->addScript('rokmediaqueries.js');
	}

	if ($gantry->get('loadtransition')) {
		$gantry->addScript('load-transition.js');
		$hidden = ' class="rt-hidden"';
	}

      //  if ($gantry->countModules('gplus')) {
	//	$gantry->addScript('GooglePlusExplicit.js', 'text/javascript', $defer=true);
//	}

	$gantry->addScript('sticky.js', 'text/javascript', $defer=true, $async=true);
    ?>
</head>
<body <?php echo $gantry->displayBodyTag(); ?>>
    <?php /** Begin Top Surround **/ if ($gantry->countModules('top') or $gantry->countModules('header')) : ?>
    <header id="rt-tophead">
        <?php /** Begin Top **/ if ($gantry->countModules('top')) : ?>
	<div id="rt-top-surround-pattern">
		<div id="rt-top-surround">
			<div id="rt-top" <?php echo $gantry->displayClassesByTag('rt-top'); ?>>
				<div class="rt-container">
					<?php echo $gantry->displayModules('top','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<?php /** Else Top **/ else : ?>
	<div id="rt-top-surround-pattern">
		<div id="rt-top-surround">

		</div>
	</div>
	<?php /** End Top **/ endif; ?>
	<?php /** Begin Header **/ if ($gantry->countModules('header')) : ?>
	<div id="rt-header-surround-pattern">
		<div id="rt-header-surround">
			<div id="rt-header">
				<div class="rt-container">
					<?php echo $gantry->displayModules('header','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<?php /** End Header **/ endif; ?>

     </header>
	<?php /** End Top Surround **/ endif; ?>
	<?php /** Begin Drawer **/ if ($gantry->countModules('drawer')) : ?>
    <div id="rt-drawer">
        <div class="rt-container">
            <?php echo $gantry->displayModules('drawer','standard','standard'); ?>
            <div class="clear"></div>
        </div>
    </div>
    <?php /** End Drawer **/ endif; ?>
	<?php /** Begin Showcase **/ if ($gantry->countModules('showcase')) : ?>
	<div id="rt-showcase-pattern" >
		<div id="rt-showcase">
			<div class="rt-container">
				<?php echo $gantry->displayModules('showcase','standard','standard'); ?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<?php /** End Showcase **/ endif; ?>
	<div id="rt-transition"<?php if ($gantry->get('loadtransition')) echo $hidden; ?>>
		<div id="rt-mainbody-surround">
  		<div id="rt-mainbody-surround-pattern">
			<?php /** Begin Feature **/ if ($gantry->countModules('feature')) : ?>
			<div id="rt-feature">
				<div class="rt-container">
					<?php echo $gantry->displayModules('feature','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Feature **/ endif; ?>
			<?php /** Begin Utility **/ if ($gantry->countModules('utility')) : ?>
			<div id="rt-utility">
				<div class="rt-container">
					<?php echo $gantry->displayModules('utility','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Utility **/ endif; ?>
			<?php /** Begin Breadcrumbs **/ if ($gantry->countModules('breadcrumb')) : ?>
			<div id="rt-breadcrumbs">
				<div class="rt-container">
					<?php echo $gantry->displayModules('breadcrumb','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Breadcrumbs **/ endif; ?>
			<?php /** Begin Main Top **/ if ($gantry->countModules('maintop')) : ?>
			<div id="rt-maintop">
				<div class="rt-container">
					<?php echo $gantry->displayModules('maintop','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Main Top **/ endif; ?>
			<?php /** Begin Full Width**/ if ($gantry->countModules('fullwidth')) : ?>
			<div id="rt-fullwidth">
				<?php echo $gantry->displayModules('fullwidth','basic','basic'); ?>
					<div class="clear"></div>
				</div>
			<?php /** End Full Width **/ endif; ?>
			<?php /** Begin Main Body **/ ?>
			<div class="rt-container">
		    		<?php echo $gantry->displayMainbody('mainbody','sidebar','standard','standard','standard','standard','standard'); ?>
		    	</div>
			<?php /** End Main Body **/ ?>
			<?php /** Begin Main Bottom **/ if ($gantry->countModules('mainbottom')) : ?>
			<div id="rt-mainbottom">
				<div class="rt-container">
					<?php echo $gantry->displayModules('mainbottom','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Main Bottom **/ endif; ?>
			<?php /** Begin Extension **/ if ($gantry->countModules('extension')) : ?>
			<div id="rt-extension">
				<div class="rt-container">
					<?php echo $gantry->displayModules('extension','standard','standard'); ?>
					<div class="clear"></div>
				</div>
			</div>
			<?php /** End Extension **/ endif; ?>
		</div>
		</div>
	</div>
	<?php /** Begin Bottom **/ if ($gantry->countModules('bottom')) : ?>
	<div id="rt-bottom">
		<div class="rt-container">
			<?php echo $gantry->displayModules('bottom','standard','standard'); ?>
			<div class="clear"></div>
		</div>
	</div>
	<?php /** End Bottom **/ endif; ?>
	<?php /** Begin Footer Section **/ if ($gantry->countModules('footer') or $gantry->countModules('copyright')) : ?>
	<footer id="rt-footer-surround">
		<?php /** Begin Footer **/ if ($gantry->countModules('footer')) : ?>
		<div id="rt-footer">
			<div class="rt-container">
				<?php echo $gantry->displayModules('footer','standard','standard'); ?>
				<div class="clear"></div>
			</div>
		</div>
		<?php /** End Footer **/ endif; ?>
		<?php /** Begin Copyright **/ if ($gantry->countModules('copyright')) : ?>
		<div id="rt-copyright">
			<div class="rt-container">
				<?php echo $gantry->displayModules('copyright','standard','standard'); ?>
				<div class="clear"></div>
			</div>
		</div>
		<?php /** End Copyright **/ endif; ?>
	</footer>
	<?php /** End Footer Surround **/ endif; ?>

	<?php /** Begin Debug **/ if ($gantry->countModules('debug')) : ?>
	<div id="rt-debug">
		<div class="rt-container">
			<?php echo $gantry->displayModules('debug','standard','standard'); ?>
			<div class="clear"></div>
		</div>
	</div>
	<?php /** End Debug **/ endif; ?>

	<?php /** Begin Analytics **/ if ($gantry->countModules('analytics')) : ?>
	   <?php echo $gantry->displayModules('analytics','basic','basic'); ?>
	<?php /** End Analytics **/ endif; ?>
	<?php /** Begin Google Plus **/ // if ($gantry->countModules('googleplus')) : ?>
	   <?php // echo $gantry->displayModules('googleplus','basic','basic'); ?>
	<?php /** End Google Plus **/ // endif; ?>
	</body>
</html>
<?php
  $gantry->finalize();
?>

<?php
/**
* @package    CDU Common Template
* @author     Christoph Hagedorn
* @copyright  Copyright © 2007 Free Software Foundation, Inc. <http://fsf.org />
* @license    GNU/GPL, see license.php
*
 * @version   $Id: analytics.php 19328 2014-02-28 18:53:02Z btowles $
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2014 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
**
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.*
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
* CDU Common Template uses the Gantry Framework (http://www.rockettheme.com), a GNU/GPLv2 web platform theme framework
* Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
**/

defined('JPATH_BASE') or die();

gantry_import('core.gantryfeature');

/**
 * @package     gantry
 * @subpackage  features
 */
class GantryFeatureGplus extends GantryFeature
{

  /*
     Please refer to js/GooglePlusExplicit.js
                     templateOptions.xml
                     index.php
  */

	var $_feature_name = 'googleplus';

	function init()
	{
		/** @var $gantry Gantry */
		global $gantry;

		ob_start();
		// start of Google Plus javascript
	?>
		window.___gcfg = {
			lang: 'de-DE',
			parsetags: 'explicit'
		};

		(function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/plusone.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();
	<?php
		// end of Google Plus javascript
		$gantry->addInlineScript(ob_get_clean());
	}


	function isInPosition($position){
		return $this->get('enabled');
	}

	function isEnabled() {
		return $this->get('enabled');
	}


	function render($position) {

		?>
		<a href="#" onClick="renderPlusone();"></a>
    		<div id="plusone-div"></div>
		<?php
	}
}

